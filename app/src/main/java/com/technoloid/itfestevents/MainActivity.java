package com.technoloid.itfestevents;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int numberOfClikcs = 0;
    private EditText nameInput;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = getSharedPreferences("MY_PREFS", MODE_PRIVATE);
        nameInput = (EditText) findViewById(R.id.nameInput);
        updateName();
    }

    private void updateName() {
        String userName = pref.getString("Name", null);
        if (userName != null) {
            TextView nameView = (TextView) findViewById(R.id.textViewName);
            nameView.setText(userName);
        }
    }

    public void buttonClicked(View view) {
        Log.d("ITFEST", "Button clicked function");
        Intent intent = new Intent(this, DetailActivity.class);
        ImageView imageView = (ImageView) findViewById(R.id.transitionImage);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, imageView, "transitionImage");
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    public void goToWeb(View view) {
//        Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse("https://facebook.com"));
//        startActivity(intent);
        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);
    }

    public void saveText(View view) {
        //save text

        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Name", nameInput.getText().toString());
        editor.apply();
        updateName();
    }

    @Override
    public void onBackPressed() {
        numberOfClikcs ++;
        if (numberOfClikcs == 1) {
            Toast.makeText(this, "Press back again to exit!", Toast.LENGTH_LONG).show();
        } else {
        }
    }
}
