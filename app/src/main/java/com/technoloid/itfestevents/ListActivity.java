package com.technoloid.itfestevents;

import android.content.Intent;
import android.icu.text.LocaleDisplayNames;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    List<EventItem> events = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ListView listView = (ListView) findViewById(R.id.listView);

        events.add(new EventItem("Android WP", "Learn Android", "06/04/2017"));
        events.add(new EventItem("Microsoft", "Learn Android", "06/04/2017"));
        events.add(new EventItem("Embedded", "Learn Android", "06/04/2017"));
        events.add(new EventItem("JavaScript", "Learn Android", "06/04/2017"));
        events.add(new EventItem("C#", "Learn Android", "06/04/2017"));
        events.add(new EventItem("PHP", "Learn Android", "06/04/2017"));
        events.add(new EventItem("NodeJS", "Learn Android", "06/04/2017"));

        EventAdapter adapter = new EventAdapter(this, events);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventItem clickedItem = events.get(position);
                Log.d("LIST", "Clicked on " + clickedItem.getEventName());
                Intent intent = new Intent(ListActivity.this, DetailActivity.class);
                intent.putExtra("TITLE", clickedItem.getEventName());
                intent.putExtra("DESCRIPTION", clickedItem.getEventDescription());
                startActivity(intent);
            }
        });
    }
}
