package com.technoloid.itfestevents;

/**
 * Created by validraganescu on 06/04/2017.
 */

public class EventItem {
    private String eventName;
    private String eventDescription;
    private String eventDate;

    public EventItem(String eventName, String eventDescription, String eventDate) {
        this.eventName = eventName;
        this.eventDescription = eventDescription;
        this.eventDate = eventDate;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public String getEventDate() {
        return eventDate;
    }
}
